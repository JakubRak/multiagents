class Agent(object):
    # # # Simple parameters # # #
    # agent ID
    agent_id = 0

    # how active is the agent (Number 1 ... inf)
    activity_strenght = 0

    # # # Full friendship net simulation # # #
    
    # list of friends (Agents) and the strength of the friendship (Number 0...1)
    friends = []
    friendship_strength = []

    # what is the agent like? 2 or more parameters (0...1)
    characteristic = []

    # how many fellows at most?
    friends_max = 0

    # # # Wallet data # # #

    # how many virtuals (3 at most) is in the wallet
    wallet_virtuals = 3
    wallet_standard = 0


    def __init__(self, args={}):
        self.wallet_standard = 0
        self.friends = []
        self.friendship_strength = []
        self.characteristic = []

        for key in args.keys():
            self.__setattr__(key, args[key])

        return

    def add_friend(self, friend, strength):
        if friend not in self.friends:
            self.friendship_strength.append(strength)
            self.friends.append(friend)
        else:
            index = self.friends.index(friend)
            self.friendship_strength[index] += strength
        return

    def remove_friend(self, friend):
        if type(friend) is not int:
            return self.remove_friend(self.friends.index(friend))
        if friend == -1:
            return False
        self.friendship_strength.pop(friend)
        self.friends.pop(friend)
        return True

    def reset_virtuals(self):
        self.wallet_virtuals = 3

    def get_wallet_amount(self):
        return self.wallet_virtuals + self.wallet_standard

    def transfer_to(self, agent, amount):
        # standard transfer
        if self.get_wallet_amount() < amount:
            return False
        agent.wallet_standard += amount
        self.wallet_virtuals -= amount
        if self.wallet_virtuals < 0:
            self.wallet_standard += self.wallet_virtuals
            self.wallet_virtuals = 0

        # apply strength
        self.add_friend(agent, amount*10)
        agent.add_friend(self, amount*20)

        return True