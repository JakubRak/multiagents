from json import dumps
from math import sqrt
from os import mkdir
from random import gauss, randint

from numpy.random import exponential as random_exponential
from numpy.random import power as random_power

from agents import Agent
from analysis import DataAnalyser
from utilities import choice_probabilities

class Market(object):

    # if the simulation should plot partial results
    debug = True
    indent = "  "
    
    # days of simulation
    day = 0

    # number of transactions for the day
    transactions = 0

    # list of active agents
    agents = []

    # simulation versions
    version_create_agent_activity = 0
        # 1 - value of 1
        # 2 - random int [1, 100]
        # 3 - random int(power(1, inf))
    version_transaction_agent_source = 0
        # 1 - equal chance for any agent ("activity" == 1)
        # 2 - random chance for agent ("activity" == randint(1...100))
        # 3 - random chance for agent ("activity" == rand_exp(1...inf))
    version_transaction_agent_target = 0
        # 1 - equal chance for any agent ("activity" == 1)
        # 2 - random chance for agent ("activity" == randint(1...100))
        # 3 - random chance for agent ("activity" == rand_exp(1...inf))
        # 4 - changes based from friends list, plus standard "10" for everyone
    version_transaction_amount = 0
        # 1 - try between 1 and 5
        # 2 - only virtuals, if no virtuals, then 1
        # 3 - two possibilities:
            # 2/3 of cases - only virtuals (if no virtuals, then 1)
            # 1/3 of cases - proportion of standard as random_power

    version_agents_number = 0
        # 1 - half of initials, no increase
        # 2 - half of initials, half speed of increase
        # 3 - normal behaviour
        # 4 - double of initials, double speed of increase
    version_number_of_transactions = 0
        # 1 - quater of initials
        # 2 - half of initials
        # 3 - normal behaviour
        # 4 - double of initials
        # 5 - quadruple of initials

    # saving progress properties
    folder_path = ""

    historical_data = {}
    # dict for days:
        # historical_data = {1: {}, 2: {}}
        # dict:
            # 1: {'transactions': [], wallet_states: []}
            # transactions: [[X1, X2, X3], [Y1, Y2, Y3], ...]
                # [agent_index_from, agent_index_to, amount]
            # wallet_states: [[X0, X1, X2, X1+X2], [Y0, Y1, Y2, Y1+Y2]]
                # [agent_index, wallet_reals, wallet_virtuals, wallet_sum]

    def __init__(self, versions={}):

        # set default versions
        values = {
            'create_agent_activity': 3,
            'transaction_agent_source': 3,
            'transaction_agent_target': 5,
            'transaction_amount': 3,
            'agents_number': 3,
            'number_of_transactions': 3
        }
        for key in values.keys():
            if key in versions:
                values[key] = versions[key]
            self.__setattr__('version_' + key, values[key])

        self.reset_market()
        self.init_day_zero()
        self.initialize_saving_point()
        return

    # # # functions that belongs to the simulation itself
    def generate_agent_parameters(self):
        result = {}
        result['agent_id'] = len(self.agents)

        if self.version_create_agent_activity == 1:
            result['activity_strenght'] = 1
        elif self.version_create_agent_activity == 2:
            result['activity_strenght'] = randint(1, 100)
        elif self.version_create_agent_activity == 3:
            strength = max(1, int(random_exponential(1.13) * 1000))
            result['activity_strenght'] = strength

        return result

    def add_agent(self):
        parameters = self.generate_agent_parameters()
        a = Agent(parameters)
        self.agents.append(a)
        return a

    def model_number_of_agents(self):
        var_a = 0.05689
        var_b = 29.040

        model = self.version_agents_number

        if model < 3:
            var_b *= 0.5
        elif model == 4:
            var_b *= 2

        if model == 1:
            var_a = 0
        elif model == 2:
            var_a *= 0.5
        elif model == 4:
            var_a *= 2

        result = var_a * self.day + var_b
        result = round(result)
        return result

    def model_number_of_transactions(self):
        if self.day % 7 == 0:
            return 0
        if self.day % 7 == 6:
            return 0
        var_a = 10.17
        var_b = sqrt(42.05)

        model = self.version_number_of_transactions
        model = 2**(model-3)

        result = gauss(var_a, var_b)
        result *= model
        result = round(result)
        result = max(0, result)
        return result

    def pick_source_agent(self):
        if self.version_transaction_agent_source in [1, 2, 3]:
            agents_activity_list = []
            for agent in self.agents:
                agents_activity_list.append(agent.activity_strenght)
            if sum(agents_activity_list) == 0:
                return 0
            return choice_probabilities(list(range(len(self.agents))), agents_activity_list)
        return 0

    def pick_target_agent(self, source_agent):
        if self.version_transaction_agent_target in [1, 2, 3]:
            agents_activity_list = []

            for agent in self.agents:
                agents_activity_list.append(agent.activity_strenght)

            if sum(agents_activity_list) == 0:
                return 0

            return choice_probabilities(list(range(len(self.agents))), agents_activity_list)

        elif self.version_transaction_agent_target in [4, 5]:
            agents_activity_list = []

            for agent in self.agents:
                activity = 0

                if self.version_transaction_agent_target == 4:
                    activity += 1 # base activity
                if self.version_transaction_agent_target == 5 and source_agent.friends == []:
                    activity += 1 # base activity when no friends

                if agent in source_agent.friends:
                    index = source_agent.friends.index(agent)
                    activity += source_agent.friendship_strength[index]

                agents_activity_list.append(activity)

            return choice_probabilities(list(range(len(self.agents))), agents_activity_list)
        return 0

    def pick_amount(self, source_agent):
        if self.version_transaction_amount == 1:
            return randint(1, 5)
        if self.version_transaction_amount == 2:
            if source_agent.wallet_virtuals > 0:
                return randint(1, source_agent.wallet_virtuals)
            return 1
        if self.version_transaction_amount == 3:
            if randint(1, 3) < 3:
                if source_agent.wallet_virtuals > 0:
                    return randint(1, source_agent.wallet_virtuals)
                return 1
            wallet_amount = source_agent.wallet_standard
            proportion = random_power(0.071)
            wallet_amount = round(wallet_amount * proportion) + source_agent.wallet_virtuals
            return wallet_amount
        return 1

    def do_transaction(self):
        # agents
        source_agent_index = self.pick_source_agent()
        source_agent = self.agents[source_agent_index]
        target_agent_index = self.pick_target_agent(source_agent)
        target_agent = self.agents[target_agent_index]
        if source_agent_index == target_agent_index:
            if self.debug:
                text = "Transaction failed: same source and target (%d)"
                text = self.indent*3 + text % source_agent_index
                print(text)
            return False

        # amount
        amount = self.pick_amount(source_agent)
        if amount == 0:
            if self.debug:
                text = "Transaction failed: amount can't be 0"
                print(text)
            return False

        if source_agent.get_wallet_amount() < amount:
            if self.debug:
                text = "Transaction failed: not enough money (%d < %d)"
                text = self.indent*3 + text % (source_agent.get_wallet_amount(), amount)
                print(text)
            return False

        # transfer
        result = source_agent.transfer_to(target_agent, amount)
        if not result:
            if self.debug:
                text = "Transaction failed: not expected error [%d/%d, %d/%d, %d]"
                text = self.indent*3 + text % (source_agent_index,
                    source_agent.get_wallet_amount(), target_agent_index,
                    target_agent.get_wallet_amount(), amount)
                print(text)
            return False

        if self.debug:
            text = "Transaction done: [%d] ==> [%d]: %d$ (%d, %d)"
            text = self.indent*3 + text % (source_agent_index,
                target_agent_index, amount, source_agent.get_wallet_amount(),
                target_agent.get_wallet_amount())
            print(text)

        transaction_data = [source_agent_index, target_agent_index, amount]
        self.historical_data[self.day]['transactions'].append(transaction_data)

        return True

    def reset_market(self):
        self.day = 0
        self.transactions = 0
        self.agents = []
        self.folder_path = ""
        self.historical_data = {}
        return

    def init_day_zero(self):
        target_agents = self.model_number_of_agents()
        count = 0
        while len(self.agents) < target_agents:
            self.add_agent()
            count += 1
        if self.debug:
            text = self.indent + "Initialized %d agents (%d tries)" 
            text = text % (len(self.agents), count)
            print(text)

        self.transactions = 0
        return

    def init_next_day(self):
        self.day += 1
        if self.debug:
            text = self.indent + "Starting day %d" % self.day
            print(text)

        # add agent
        target_agents = self.model_number_of_agents()
        agents_diff = target_agents - len(self.agents)
        count = 0
        while len(self.agents) < target_agents:
            self.add_agent()
            count += 1
        if self.debug:
            text = self.indent*2 + "Added %d agents (%d tries)" 
            text = text % (agents_diff, count)
            print(text)

        # reset virtuals
        for itr in range(len(self.agents)):
            self.agents[itr].reset_virtuals()

        # make transactions
        transactions = self.model_number_of_transactions()
        self.transactions = transactions
        if self.debug:
            text = self.indent*2 + "Planned %d transactions" % transactions
            print(text)

        self.historical_data[self.day] = {
            'transactions': []
        }

        return

    def run_transactions(self):
        count_all = 0
        count_done = 0
        while count_done < self.transactions:
            result = self.do_transaction()
            if result:
                count_done += 1
            count_all += 1

        if self.debug:
            text = self.indent*2 + "%d over %d steps were correct (%d%%)" 
            factor = 100
            if count_all > 0:
                factor = 100*count_done/count_all
            text = text % (count_done, count_all, factor)
            print(text)

        return

    # # # functions saving state
    def initialize_saving_point(self):
        path = "results/counter.dat"
        
        file = open(path, "r")
        counter = int(file.read())
        file.close()
        
        counter += 1
        
        file = open(path, "w")
        file.write(str(counter))
        file.close()

        self.folder_path = "results/%03d/" % counter
        mkdir(self.folder_path)

        self.historical_data = {}

        # save info about version
        versions = {
            'create_agent_activity': self.version_create_agent_activity,
            'transaction_agent_source': self.version_transaction_agent_source,
            'transaction_agent_target': self.version_transaction_agent_target,
            'transaction_amount': self.version_transaction_amount,
            'agents_number': self.version_agents_number,
            'number_of_transactions': self.version_number_of_transactions,
        }
        file = open(self.folder_path + "versions.json", "w")
        file.write(dumps(versions, indent=3, sort_keys=True))
        file.close()

        return counter

    def collect_historical_data(self):
        result = []
        for itr in range(len(self.agents)):
            agent = self.agents[itr]
            new_row = [itr]
            new_row.append(agent.wallet_standard)
            new_row.append(agent.wallet_virtuals)
            new_row.append(agent.get_wallet_amount())
            result.append(new_row)
        self.historical_data[self.day]['wallet_states'] = result
        return

    # # # functions that run simulation
    def run_next_day(self):
        self.init_next_day()
        self.run_transactions()
        self.collect_historical_data()
        return

    def simulate_market_flow(self, number_of_days):
        counter = 0
        while counter < number_of_days:
            self.run_next_day()
            counter += 1

        file = open(self.folder_path + "historical_data.json", "w")
        file.write(dumps(self.historical_data, indent=3, sort_keys=True))
        file.close()

        da = DataAnalyser(self.historical_data, self.folder_path)
        # da.debug = self.debug
        da.indent = self.indent
        da.analyse_data()
        return True