from random import choice

def choice_probabilities(values, probabilities):
    if len(values) != len(probabilities):
        return 0
    source = []
    for itr in range(len(values)):
        source += [values[itr]] * probabilities[itr]
    return choice(source)
