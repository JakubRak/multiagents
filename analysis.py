from copy import deepcopy
from math import exp, log
from os import mkdir
from json import dumps

import matplotlib.pyplot as plt

class DataAnalyser(object):
    plt = ""

    path = ""
    historical_data = ""

    debug = True
    indent = "  "

    settings = {}

    output_file = ""

    def __init__(self, historical_data, folder_path):
        super(DataAnalyser, self).__init__()
        self.plt = plt

        self.path = folder_path
        self.historical_data = historical_data

        self.init_folders()
        self.init_settings()
        self.init_files()

        return

    def init_folders(self):
        folder_names = [
            "day_wallets_sorted",
            "plots", "extracts"]
        for name in folder_names:
            mkdir(self.path + name + "/")
        return

    def init_files(self):
        output_file = open(self.path + "extracts/output.txt", "w")
        self.output_file = output_file
        return

    def close_files(self):
        self.output_file.close()

    def init_settings(self):
        settings = {}
        settings['plots_time_delta'] = 7 # how many days between each day-plot
        self.settings = settings
        return

    def plot_to_file(self, data, image_arguments={}):
        # data - list of lists to plot
        # image_arguments - dict of arguments that will be used
        plt = self.plt
        plt.clf()

        # default values
        defaults = {'title': None, 'labels_rotation': 45, 'dpi': 200,
            'x_fontsize': 10, 'title_fontsize': 12,
            'x_label': None, 'y_label': None, 'plot_type': 'line',
            'point_size': 2, # size of dots in scatter plot (plot type = "point")
            'histogram_bins': None, 'histogram_range': None, # data for histogram plot
            'model_fit': None, # data for curve/line fitting, as usual line plot
            'bar_width': 1.0, # width bars in bar plot
            'filename': "test"
            }
        for key in defaults.keys():
            if key not in image_arguments:
                image_arguments[key] = defaults[key]
        # if not image_arguments['filename'].endswith(".png"):
        #     image_arguments['filename'] += ".png"

        # plot values
        if image_arguments['plot_type'] == "line":
            for single_data in data:
                plt.plot(single_data[0], single_data[1])
        elif image_arguments['plot_type'] == "point":
            for single_data in data:
                plt.scatter(single_data[0], single_data[1],
                    s=image_arguments['point_size'])
        elif image_arguments['plot_type'] == "histogram":
            for single_data in data:
                plt.hist(single_data,
                    bins=image_arguments['histogram_bins'],
                    range=image_arguments['histogram_range'])
        elif image_arguments['plot_type'] == "bar":
            bottom = [0] * len(data[0][0])
            for single_data in data:
                plt.bar(single_data[0], single_data[1],
                    bottom=bottom, width=image_arguments['bar_width'])
                for itr in range(len(single_data[1])):
                    bottom[itr] += single_data[1][itr]
        else:
            for single_data in data:
                plt.plot(single_data[0], single_data[1])

        # curve/line model fit
        if image_arguments['model_fit'] is not None:
            plt.plot(image_arguments['model_fit'][0], 
                image_arguments['model_fit'][1], 'k', linewidth=0.5)
            if len(image_arguments['model_fit']) > 2:
                plt.plot(image_arguments['model_fit'][0], 
                    image_arguments['model_fit'][2], 'b', linewidth=0.5)

        # other data
        if image_arguments['title'] is not None:
            plt.title(label=str(image_arguments['title']), loc='center',
                    fontsize=image_arguments['title_fontsize'])
        plt.xticks(rotation=image_arguments['labels_rotation'], ha="right",
                fontsize=image_arguments['x_fontsize'])
        if image_arguments['x_label'] is not None:
            plt.xlabel(image_arguments['x_label'])
        if image_arguments['y_label'] is not None:
            plt.ylabel(image_arguments['y_label'])
        plt.tight_layout()

        # saving image
        plt.savefig(self.path + image_arguments['filename'] + ".png", 
            dpi=image_arguments['dpi'])
        plt.savefig(self.path + image_arguments['filename'] + ".eps", 
            dpi=image_arguments['dpi'])
        return

    def extract_to_file(self, data, filename):
        file = open(self.path + filename, "w")

        new_data = str(data)
        if type(data) is not str:
            new_data = dumps(data, indent=2)

        file.write(new_data)
        file.close()
        return

    def wallets_per_day(self):
        counter = -1
        time_delta = self.settings['plots_time_delta']
        for key in self.historical_data.keys():
            counter += 1
            if counter % time_delta != 0:
                continue
            day_data = deepcopy(self.historical_data[key]['wallet_states'])
            day_data.sort(key = lambda x: -x[3])
            
            arguments = []
            standard = []
            virtuals = []

            for el in day_data:
                arguments.append("ID%03d" % el[0])
                standard.append(el[1])
                virtuals.append(el[2])

            image_data = [[arguments, standard], [arguments, virtuals]]
            filename = "day_wallets_sorted/day_%03d" % key
            image_arguments = {'plot_type': 'bar', 'filename': filename}
            image_arguments["x_label"] = "ID agenta"
            image_arguments["y_label"] = "Wartość portfela"
            image_arguments["title"] = "Posortowane wartości portfeli"
            self.plot_to_file(image_data, image_arguments)
        return

    def pareto_principle_plot(self):
        # data1 - 80% of money, data2 - 20% of richest
        data1 = [
            [[], []],
            [[], []]
            ]
        data2 = [
            [[], []],
            [[], []]
            ]
        for key in self.historical_data.keys():
            day_data = deepcopy(self.historical_data[key]['wallet_states'])
            day_data.sort(key = lambda x: -x[3])

            total_money = sum([day_data[i][3] for i in range(len(day_data))])
            target_money = 0.8 * total_money
            flag_money = True
            count_money = 0

            total_people = len(day_data)
            target_people = 0.2 * total_people
            flag_people = True
            count_people = 0

            for el in day_data:
                value = el[3]

                if flag_money:
                    if target_money < value:
                        count_money += target_money / value
                        flag_money = False
                        data1[0][0].append(key)
                        data1[0][1].append(count_money / total_people)
                        data1[1][0].append(key)
                        data1[1][1].append(0.2)
                    else:
                        count_money += 1
                        target_money -= value

                if flag_people:
                    if target_people <= 0:
                        flag_people = False
                        data2[0][0].append(key)
                        data2[0][1].append(count_people / total_money)
                        data2[1][0].append(key)
                        data2[1][1].append(0.8)
                    elif target_people <= 1:
                        count_people += target_people * value
                        flag_people = False
                        data2[0][0].append(key)
                        data2[0][1].append(count_people / total_money)
                        data2[1][0].append(key)
                        data2[1][1].append(0.8)
                    else:
                        count_people += value
                        target_people -= 1

                if not (flag_money or flag_people):
                    break

        filename1 = "pareto_80%_of_money"
        filename2 = "pareto_20%_of_people"
        image_arguments = {'plot_type': 'line'}

        image_arguments['filename'] = "plots/" + filename1
        image_arguments["x_label"] = "Czas symulacji [dni]"
        image_arguments["title"] = "Spełnienie prawa Pareto w czasie symulacji"

        image_arguments["y_label"] = "Najbogatsi posiadacze 80% majątku do wszystkich agentów"
        self.plot_to_file(data1, image_arguments)
        self.extract_to_file(data1, 'extracts/' + filename1 + ".dat")

        image_arguments["y_label"] = "Zasoby 20% najbogatszych agentów do całości wolumenu"
        image_arguments['filename'] = "plots/" + filename2
        self.plot_to_file(data2, image_arguments)
        self.extract_to_file(data2, 'extracts/' + filename2 + ".dat")

        return

    def d_sum_fit_powerlaw(self, data_to_fit, alpha):
        result = 0
        for itr in range(len(data_to_fit)):
            x = itr + 1
            y = data_to_fit[itr]
            x_alpha = x**(-alpha)
            x_log = log(x)
            
            v1 = x_alpha
            v2 = y - (alpha-1) * x_alpha
            v3 = -1 + (alpha-1) * alpha * x_log
            partial = v1 * v2 * v3

            result += partial
        return result

    def powerlaw_fit(self, data_to_fit):
        v1 = 0
        v2 = 3
        s1 = self.d_sum_fit_powerlaw(data_to_fit, v1)
        s2 = self.d_sum_fit_powerlaw(data_to_fit, v2)
        if s1 > 0 or s2 < 0:
            print("Unable to find minimum")
            return False

        delta = 10**-10

        for itr in range(40):
            vN = (v1 + v2) / 2
            sN = self.d_sum_fit_powerlaw(data_to_fit, vN)
            if sN < 0:
                s1 = sN
                v1 = vN
            else:
                s2 = sN
                v2 = vN
        return vN

    def d_sum_fit_exp(self, data_to_fit, beta):
        result = 0
        for itr in range(len(data_to_fit)):
            x = itr
            y = data_to_fit[itr]
            expx = exp(-beta * x)
            
            v1 = beta * expx - y
            v2 = expx
            v3 = 1 - beta * x
            partial = v1 * v2 * v3

            result += partial
        return result

    def exponential_fit(self, data_to_fit):
        v1 = 0
        v2 = 3
        s1 = self.d_sum_fit_exp(data_to_fit, v1)
        s2 = self.d_sum_fit_exp(data_to_fit, v2)
        if s1 > 0 or s2 < 0:
            print("Unable to find minimum")
            return False

        for itr in range(40):
            vN = (v1 + v2) / 2
            sN = self.d_sum_fit_exp(data_to_fit, vN)
            if sN < 0:
                s1 = sN
                v1 = vN
            else:
                s2 = sN
                v2 = vN
        return vN

    def normalize_last_wallets(self):
        last_key = self.historical_data.keys()
        last_key = list(last_key)[-1]

        day_data = deepcopy(self.historical_data[last_key]['wallet_states'])
        day_data.sort(key = lambda x: -x[3])
            
        arguments = []
        standard = []
        virtuals = []
        totals = []

        for el in day_data:
            arguments.append("ID%03d" % el[0])
            standard.append(el[1])
            virtuals.append(el[2])
            totals.append(el[1] + el[2])

        multiplier = sum(totals)
        multiplier = 1/multiplier
        for itr in range(len(arguments)):
            standard[itr] *= multiplier
            virtuals[itr] *= multiplier
            totals[itr] *= multiplier

        image_data = [[arguments, standard], [arguments, virtuals]]
        filename = "plots/normalized_plain.png"
        image_arguments = {'plot_type': 'bar', 'filename': filename}
        image_arguments["y_label"] = "Znormalizowana wartość portfela"
        image_arguments["x_label"] = "ID agenta"
        image_arguments["title"] = "Znormalizowane, posortowane wartości portfeli"
        self.plot_to_file(image_data, image_arguments)

        return [arguments, totals]

    def power_fit_last_wallets(self, normalized_wallets):
        alpha = self.powerlaw_fit(normalized_wallets[1])

        result = []
        for itr in range(len(normalized_wallets[0])):
            result.append((alpha - 1) * (itr + 1) ** (- alpha))

        filename = "plots/normalized_power_fit.png"
        image_arguments = {'plot_type': 'bar', 'filename': filename}
        image_arguments['model_fit'] = [normalized_wallets[0], result]
        image_arguments["y_label"] = "Znormalizowana wartość portfela"
        image_arguments["x_label"] = "ID agenta"
        image_arguments["title"] = "Znormalizowane, posortowane wartości portfeli"
        self.plot_to_file([normalized_wallets], image_arguments)

        output = [
            "= = = = Powerlaw model fit = = = =",
            "alpha = " + str(alpha),
            "equation: f(x) = (alpha-1) * x^(-alpha)"
            ]
        output = "\n".join(output)
        output += "\n"
        self.output_file.write(output)

        return result

    def exp_fit_last_wallets(self, normalized_wallets):
        beta = self.exponential_fit(normalized_wallets[1])

        result = []
        for itr in range(len(normalized_wallets[0])):
            result.append(beta * exp(-beta * itr))

        filename = "plots/normalized_exp_fit.png"
        image_arguments = {'plot_type': 'bar', 'filename': filename}
        image_arguments['model_fit'] = [normalized_wallets[0], result]
        image_arguments["y_label"] = "Znormalizowana wartość portfela"
        image_arguments["x_label"] = "ID agenta"
        image_arguments["title"] = "Znormalizowane, posortowane wartości portfeli"
        self.plot_to_file([normalized_wallets], image_arguments)

        output = [
            "= = = = Exponential model fit = = = =",
            "beta = " + str(beta),
            "equation: f(x) = beta * exp(-beta*x)"
            ]
        output = "\n".join(output)
        output += "\n"
        self.output_file.write(output)

        return result

    def plot_both_fits(self, normalized_wallets, fits):
        filename = "plots/normalized_both_fits.png"
        image_arguments = {'plot_type': 'bar', 'filename': filename}
        image_arguments['model_fit'] = [normalized_wallets[0]]
        for el in fits:
            image_arguments['model_fit'].append(el)
        image_arguments["y_label"] = "Znormalizowana wartość portfela"
        image_arguments["x_label"] = "ID agenta"
        image_arguments["title"] = "Znormalizowane, posortowane wartości portfeli"
        self.plot_to_file([normalized_wallets], image_arguments)
        return

    def gini_calculator(self, wealth_status):
        wealth_status = deepcopy(wealth_status)
        wealth_status.sort()

        N = len(wealth_status)

        s1 = 0
        s2 = 0
        for itr in range(N):
            s1 += (itr + 1) * wealth_status[itr]
            s2 += wealth_status[itr]
        G1 = (2 * s1) / (N * s2)
        G1 -= (N + 1) / N

        # s1 = 0
        # s2 = 0
        # for itr1 in range(N):
        #     for itr2 in range(N):
        #         s1 += abs(wealth_status[itr1] - wealth_status[itr2])
        #     s2 += wealth_status[itr1]
        # s2 /= N
        # G2 = (s1) / (2 * N * N * s2)

        return G1

    def gini_coefficient_analysis(self):
        data = [
            [[], []]
            ]

        for key in self.historical_data.keys():
            day_data = deepcopy(self.historical_data[key]['wallet_states'])
            day_data.sort(key = lambda x: -x[3])

            wealth_status = [day_data[i][3] for i in range(len(day_data))]
            gini = self.gini_calculator(wealth_status)

            data[0][0].append(key)
            data[0][1].append(gini)


        filename = "plots/gini"
        image_arguments = {'plot_type': 'line', 'filename': filename}
        image_arguments["y_label"] = "Wartość współczynnika Giniego"
        image_arguments["x_label"] = "Czas symulacji [dni]"
        image_arguments["title"] = "Wartość współczynnika Giniego w czasie symulacji"
        self.plot_to_file(data, image_arguments)

        self.extract_to_file(data, 'extracts/gini.dat')

        output = [
            "= = = = Last gini value = = = =",
            "gini = " + str(gini),
            ]
        output = "\n".join(output)
        output += "\n"
        self.output_file.write(output)

        return

    def analyse_data(self):
        if type(self.historical_data) is not dict:
            print("ERROR: Historical data is not dict")
            return
        if len(list(self.historical_data.keys())) == 0:
            print("ERROR: Historical data is empty")
            return

        if self.debug:
            print("Starting data analysis...")
            print(self.indent + "Sorted day wallets...")
        self.wallets_per_day()
        if self.debug:
            print(self.indent + "Sorted day wallets done")

        if self.debug:
            print("Starting data analysis...")
            print(self.indent + "Pareto principle plot...")
        self.pareto_principle_plot()
        if self.debug:
            print(self.indent + "Pareto principle plot done")

        if self.debug:
            print(self.indent + "Normalized plots...")
        normalized_data = self.normalize_last_wallets()
        power_fit = self.power_fit_last_wallets(normalized_data)
        exp_fit = self.exp_fit_last_wallets(normalized_data)
        self.plot_both_fits(normalized_data, [power_fit, exp_fit])
        if self.debug:
            print(self.indent + "Normalized plots done")

            print(self.indent + "Gini coefficient plot...")
        self.gini_coefficient_analysis()
        if self.debug:
            print(self.indent + "Gini coefficient plot done")

        self.close_files()

        return