from market import Market

versions = {}
versions['create_agent_activity'] = 3
versions['transaction_agent_source'] = 3
versions['transaction_agent_target'] = 5
versions['transaction_amount'] = 3

versions['agents_number'] = 3 # 3
versions['number_of_transactions'] = 3 # 3

# # # # # # Agents initial status # # # # # #
# # # # DO NOT REPEAT THIS PART # # # #

# versions['agents_number'] = 3 # 3
# versions['number_of_transactions'] = 3 # 3
# versions['create_agent_activity'] = 3
# versions['transaction_agent_source'] = 3
# versions['transaction_agent_target'] = 5
# versions['transaction_amount'] = 3
# for i in range(1, 4):
#     versions['create_agent_activity'] = 1
#     m = Market(versions)
#     m.debug = False
#     m.simulate_market_flow(837)
# for i in range(1, 4):
#     versions['create_agent_activity'] = 2
#     m = Market(versions)
#     m.debug = False
#     m.simulate_market_flow(837)
# for i in range(1, 4):
#     versions['create_agent_activity'] = 3
#     m = Market(versions)
#     m.debug = False
#     m.simulate_market_flow(837)

# # # # # # Agents initial status and choices # # # # # #

# versions['agents_number'] = 3 # 3
# versions['number_of_transactions'] = 3 # 3
# versions['create_agent_activity'] = 3
# versions['transaction_agent_source'] = 3
# versions['transaction_agent_target'] = 5
# versions['transaction_amount'] = 3
# sets = [
#     [1, 1, 1],
#     [2, 2, 2],
#     [3, 3, 3],
#     [3, 3, 5],
# ]
# for el in sets:
#     versions['create_agent_activity'] = el[0]
#     versions['transaction_agent_source'] = el[1]
#     versions['transaction_agent_target'] = el[2]
#     m = Market(versions)
#     m.debug = False
#     m.simulate_market_flow(837)

# # # # # # # Agents total number # # # # # #

# versions['create_agent_activity'] = 3
# versions['transaction_agent_source'] = 3
# versions['transaction_agent_target'] = 5
# versions['transaction_amount'] = 3
# versions['agents_number'] = 3 # 3
# versions['number_of_transactions'] = 3 # 3
# for i in range(1, 5):
#     versions['agents_number'] = i
#     m = Market(versions)
#     m.debug = False
#     m.simulate_market_flow(837)


# # # # # # Number of transactions per day # # # # # #

# versions['create_agent_activity'] = 3
# versions['transaction_agent_source'] = 3
# versions['transaction_agent_target'] = 5
# versions['transaction_amount'] = 3
# versions['agents_number'] = 3 # 3
# versions['number_of_transactions'] = 3 # 3
# for i in range(4, 6):
#     versions['number_of_transactions'] = i
#     m = Market(versions)
#     m.debug = False
#     m.simulate_market_flow(837)



m = Market(versions)
m.debug = True
m.simulate_market_flow(837)
