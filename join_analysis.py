# from copy import deepcopy
# from math import exp, log
from os import mkdir, path
from json import loads
# from json import dumps

import matplotlib.pyplot as plt

class DataJoiner(object):

    list_of_joins = []
    path = "results/collective/"

    def __init__(self, list_of_joins=[]):
        self.plt = plt

        self.list_of_joins = list_of_joins

        self.path = "results/collective/"
        self.init_folder()

        return

    def init_folder(self):
        print(self.list_of_joins)
        folder_name = ",".join(self.list_of_joins)
        self.path += folder_name + "/"
        if(not path.isdir(self.path)):
            mkdir(self.path)
        return

    def get_file_content(self, path):
        file = open(path, "r")
        content = file.read()
        file.close()
        return loads(content)

    def plot_to_file(self, data, image_arguments={}):
        # data - list of lists to plot
        # image_arguments - dict of arguments that will be used
        plt = self.plt
        plt.clf()

        # default values
        defaults = {'title': None, 'labels_rotation': 45, 'dpi': 200,
            'x_fontsize': 10, 'title_fontsize': 12,
            'x_label': None, 'y_label': None, 'plot_type': 'line',
            'point_size': 2, # size of dots in scatter plot (plot type = "point")
            'histogram_bins': None, 'histogram_range': None, # data for histogram plot
            'model_fit': None, # data for curve/line fitting, as usual line plot
            'bar_width': 1.0, # width bars in bar plot
            'filename': "test"
            }
        for key in defaults.keys():
            if key not in image_arguments:
                image_arguments[key] = defaults[key]
        # if not image_arguments['filename'].endswith(".eps"):
        #     image_arguments['filename'] += ".eps"

        # plot values
        if image_arguments['plot_type'] == "line":
            for single_data in data:
                plt.plot(single_data[0], single_data[1])
        elif image_arguments['plot_type'] == "point":
            for single_data in data:
                plt.scatter(single_data[0], single_data[1],
                    s=image_arguments['point_size'])
        elif image_arguments['plot_type'] == "histogram":
            for single_data in data:
                plt.hist(single_data,
                    bins=image_arguments['histogram_bins'],
                    range=image_arguments['histogram_range'])
        elif image_arguments['plot_type'] == "bar":
            bottom = [0] * len(data[0][0])
            for single_data in data:
                plt.bar(single_data[0], single_data[1],
                    bottom=bottom, width=image_arguments['bar_width'])
                for itr in range(len(single_data[1])):
                    bottom[itr] += single_data[1][itr]
        else:
            for single_data in data:
                plt.plot(single_data[0], single_data[1])

        # curve/line model fit
        if image_arguments['model_fit'] is not None:
            plt.plot(image_arguments['model_fit'][0], 
                image_arguments['model_fit'][1], 'k', linewidth=0.5)
            if len(image_arguments['model_fit']) > 2:
                plt.plot(image_arguments['model_fit'][0], 
                    image_arguments['model_fit'][2], 'b', linewidth=0.5)

        # other data
        if image_arguments['title'] is not None:
            plt.title(label=str(image_arguments['title']), loc='center',
                    fontsize=image_arguments['title_fontsize'])
        plt.xticks(rotation=image_arguments['labels_rotation'], ha="right",
                fontsize=image_arguments['x_fontsize'])
        if image_arguments['x_label'] is not None:
            plt.xlabel(image_arguments['x_label'])
        if image_arguments['y_label'] is not None:
            plt.ylabel(image_arguments['y_label'])
        plt.tight_layout()

        # saving image
        plt.savefig(self.path + image_arguments['filename']+".eps", 
            dpi=image_arguments['dpi'])
        plt.savefig(self.path + image_arguments['filename']+".png", 
            dpi=image_arguments['dpi'])
        return

    def join_ginis(self):
        ginis = []
        for target in self.list_of_joins:
            file_name = "results/" + target + "/extracts/gini.dat"
            ginis.append(self.get_file_content(file_name)[0])

        image_arguments = {'plot_type': 'line',
            'filename': 'ginis'}
        image_arguments["y_label"] = "Wartość współczynnika Giniego"
        image_arguments["x_label"] = "Czas symulacji [dni]"
        image_arguments["title"] = "Wartość współczynnika Giniego w czasie symulacji"
        self.plot_to_file(ginis, image_arguments)
        return

    def join_pareto_20(self):
        paretos = []
        file_content = []
        for target in self.list_of_joins:
            file_name = "results/" + target + "/extracts/pareto_20%_of_people.dat"
            file_content = self.get_file_content(file_name)
            paretos.append(file_content[0])

        image_arguments = {'plot_type': 'line',
            'filename': 'pareto_20%_of_people'}
        image_arguments["x_label"] = "Czas symulacji [dni]"
        image_arguments["title"] = "Spełnienie prawa Pareto w czasie symulacji"

        image_arguments["y_label"] = "Zasoby 20% najbogatszych agentów do całości wolumenu"
        image_arguments['model_fit'] = file_content[1]
        self.plot_to_file(paretos, image_arguments)
        return

    def join_pareto_80(self):
        paretos = []
        file_content = []
        for target in self.list_of_joins:
            file_name = "results/" + target + "/extracts/pareto_80%_of_money.dat"
            file_content = self.get_file_content(file_name)
            paretos.append(file_content[0])

        image_arguments = {'plot_type': 'line',
            'filename': 'pareto_80%_of_money'}
        image_arguments["x_label"] = "Czas symulacji [dni]"
        image_arguments["title"] = "Spełnienie prawa Pareto w czasie symulacji"

        image_arguments["y_label"] = "Najbogatsi posiadacze 80% majątku do wszystkich agentów"
        image_arguments['model_fit'] = file_content[1]
        self.plot_to_file(paretos, image_arguments)
        # print(ginis)
        return

    def join_data(self):
        self.join_ginis()
        self.join_pareto_20()
        self.join_pareto_80()
        return

def transform_set(values):
    result = []
    for el in values:
        if type(el) is str:
            result.append(el)
        else:
            result.append("%03d" % el)
    return result

targets = [
    # [56, 57, 58],
    # [59, 60, 61],
    # [62, 63, 64],
    # [65, 66, 67, 68],
    # [69, 70, 71, 72],
    # [73, 74, 75],
    [88, 89, 90, 91],
    [92, 93, 94, 95],
    [96, 97, 98, 99],
    [101, 102, 103, 104],
    ]
for el in targets:
    dj = DataJoiner(transform_set(el))
    dj.join_data()

# targets = ["011", "012", "013", "014"]
# dj = DataJoiner(targets)
# dj.join_data()